type name = string

type binop = Add | Sub | Mul | Div | Eq | Lt | Gt

type ty =
    | TyInt
    | TyBool
    | TyArrow of ty * ty
    | TyUnit
    | TyArray of ty
    | TyVar

type fun_args = (name * ty) list

type expr =
    | Var of name
    | Int of int
    | Bool of bool
    | If of expr * expr * expr option
    | LetExpr of name * expr * expr
    | Lambda of fun_args * ty * expr
    | Fun of name * fun_args * ty * expr
    | Apply of name * expr list
    | ArrayLiteral of expr list
    | ArraySet of expr * expr
    | ArrayGet of expr * expr
    | ExprSeq of expr list
    | While of expr * expr
    | BinaryOp of binop * expr * expr

type cexpr =
    | CVar of name
    | CInt of int
    | CBool of bool
    | CIf of expr * expr * expr option
    | CLetExpr of name * expr * expr
    | CFun of name * fun_args * ty * expr
    | CApply of name * expr list
    | CArrayLiteral of expr list
    | CArraySet of expr * expr
    | CArrayGet of expr * expr
    | CExprSeq of expr list
    | CWhile of expr * expr
    | CBinaryOp of binop * expr * expr

type toplevel_def =
    | Expr of expr
    | Def of name * expr

let type_to_str = function
    | TyBool -> "Bool"
    | TyInt -> "Int"
    | TyArrow _ -> "Function"
    | TyUnit -> "()"
    | TyArray _ -> "[||]"

let binop_to_str = function
    | Add -> "+"
    | Sub -> "-"
    | Mul -> "*"
    | Div -> "/"
    | Lt -> "<"
    | Gt -> ">"
    | Eq -> "="

(* pretty print an AST *)
let rec pprint exp = match exp with
    | Var i -> "(Var " ^ i ^ ")"
    | Int n -> "(Int " ^ (string_of_int n) ^ ")"
    | Bool b -> "(Bool " ^ (string_of_bool b) ^ ")"
    | BinaryOp (op, a, b) -> "(Binop" ^ (binop_to_str op) ^ (pprint a) ^ (pprint b) ^ ")"
    | If (a, b, c) -> "(If (" ^ (pprint a) ^ ") THEN:" ^ (pprint b) ^ (match c with | Some i -> "ELSE: " ^ (pprint i) ^ ")" | None -> "")
    | Fun (name, args, ret_ty, body) -> "(Fun " ^ name ^  (pprint body) ^ ")"
    | ArrayLiteral _ -> "[| |]"
    | ArraySet _ -> "ArraySet"
    | ArrayGet _ -> "ArrayGet"
    | ExprSeq exprs -> List.fold_left (fun acc x -> acc ^ " " ^ x) "" (List.map pprint exprs)
    | Apply (name, args) -> "(Apply: " ^ name ^ ")"
    | LetExpr (name, exp, body) -> "(Let " ^ name ^ (pprint exp) ^ "in " ^ (pprint body) ^ ")"
    | Lambda (args, ty, body) -> "(Lambda "  ^ (pprint body) ^ ")"

let toplevel_to_string def = match def with
    | Def (_, e) -> pprint e
    | Expr e -> pprint e

exception TypeError of string
exception NotImplemented

let type_err message =
    raise (TypeError message)

let match_int a b = match a,b with
    | TyInt, TyInt -> TyInt
    | TyInt, x -> type_err ("expected int but got: " ^ (type_to_str x))
    | _ as x, TyInt -> type_err ("expected int but got: " ^ (type_to_str x))
    | _, _ -> type_err "Operation requires both operands to be of type integer"

let type_else f env = function
    | Some e -> f env e
    | None -> TyUnit


let type_cond cond t1 t2 =
    match cond with
        | TyBool ->
                if t1 <> t2 then
                    type_err "conditionals got different types"
                else
                    cond
        | _ as x ->
                type_err ("expected boolean but got: " ^ (type_to_str x))

let rec typeof env = function
    | Var i ->
            (try List.assoc i env with
                Not_found -> type_err ("unbound variable: " ^ i))
    | Int _ -> TyInt
    | Bool _ -> TyBool
    | If (cond, then_part, else_part) ->
            let cond_type = typeof env cond in
            let then_part_type = typeof env then_part in
            let else_part_type = type_else typeof env else_part in
            type_cond cond_type then_part_type else_part_type
    | BinaryOp (op, a, b) -> match_int (typeof env a) (typeof env b)
    | LetExpr (name, exp, body) -> typeof env body
    | Lambda (args, ty, body) -> TyArrow(ty, (typeof env body))
    | Apply _ -> raise NotImplemented

(*
 * FIXME: do full lambda lifting
let rec lambda_lift exp env = match exp with
    | Lambda (fun_args, ty, expr) -> exp
    | _ -> exp

module StringSet = Set.Make(struct
    type t = string
    let compare = Pervasives.compare
end)

let rec freevars exp fv =
    | Var i -> StringSet.add fv i
    | Int _
    | Bool _ -> StringSet.empty
    | If (cond, _then, _else) ->
            StringSet.
            *)

