grammar:
	ocamlc -c core.ml
	ocamllex lexer.mll
	ocamlyacc parser.mly
	ocamlc -c parser.mli
	ocamlc -c lexer.ml
	ocamlc -c parser.ml
	ocamlc -c c.ml

llvm: grammar
	#ocamlfind ocamlc -package llvm,llvm.bitreader emit_llvm.ml
	#ocamlfind ocamlc -g -o emit_llvm -linkpkg lexer.cmo emit_llvm.cmo core.cmo
	ocamlc -I /usr/lib/ocaml/llvm-3.5/ -c emit_llvm.ml
	ocamlc -I /usr/lib/ocaml/llvm-3.5/ -o llvm lexer.cmo parser.cmo core.cmo emit_llvm.cmo

all: grammar
	ocamlc -o compiler core.cmo lexer.cmo parser.cmo c.cmo

.PHONY : clean
clean:
	rm *.cmo
	rm *.cmi
	rm lexer.ml
	rm parser.ml
	rm parser.mli
	rm compiler
