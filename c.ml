let typecheck = function
    | Core.Expr e ->
        let ty = Core.typeof [] e in
        Core.type_to_str ty
    | Core.Def (name, expr) -> "unknown yet.."

let _ =
    try
        let lexbuf = Lexing.from_channel stdin in
        print_endline "Press CTRL-D to see the expr string representation,
        i.e. 1+2;; ENTER, CTRL-D";
        while true do
            let toplevel_defs = Parser.toplevel_def Lexer.token lexbuf in
            List.iter (fun def ->
                try
                    print_string (Core.toplevel_to_string def);
                    print_endline (":" ^ typecheck def)
                with Core.TypeError e ->
                    print_endline "" ;
                    print_endline ("Type error: " ^ e)
            ) toplevel_defs;
            flush stdout
        done
    with Lexer.Eof ->
        exit 0
