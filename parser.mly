%{
    open Core
    open Env
%}
%token TINT
%token TBOOL
%token TARROW
%token <Core.name> VAR
%token <int> INT
%token PLUS MINUS TIMES DIV
%token LPAREN RPAREN
%token EQUAL
%token IF THEN ELSE
%token FUN
%token FUNDEF
%token LET
%token COLON
%token SEMI
%token SEMICOLON2
%token EOF
%token TRUE FALSE
%token COMMA
%token LT GT DOT IN
%token LB
%token RB
%token BAR
%token LARROW
%token DO
%token DONE
%token WHILE
%token ARRAY
%token END
%token EQARROW

%start toplevel_def
%type <Core.toplevel_def list> toplevel_def

%left PLUS MINUS
%left TIMES DIV
%left COLON


%right TARROW

%%

toplevel_def:
    EOF { [] }
    | expr { [Expr $1] }
    | toplevel_def expr { Expr($2) :: $1 }

expr:
    VAR { Var $1 }
    | TRUE { Bool true }
    | FALSE { Bool false }
    | INT { Int $1 }
    | expr PLUS expr { BinaryOp (Core.Add, $1, $3) }
    | expr MINUS expr { BinaryOp (Core.Sub, $1, $3) }
    | expr TIMES expr { BinaryOp (Core.Mul, $1, $3) }
    | expr EQUAL expr { BinaryOp (Core.Eq, $1, $3) }
    | expr LT expr { BinaryOp (Core.Lt, $1, $3) }
    | expr GT expr { BinaryOp (Core.Gt, $1, $3) }
    | expr DIV expr { BinaryOp (Core.Div, $1, $3) }
    | IF expr THEN expr { If ($2, $4, None) }
    | IF expr THEN expr ELSE expr { If ($2, $4, Some $6) }
    | FUN LPAREN fun_args RPAREN EQARROW expr { Lambda ($3, Core.TyInt, $6) }
    | FUNDEF VAR LPAREN fun_args RPAREN COLON ty EQUAL expr { Fun ($2, $4, $7, $9) }
    | LET VAR EQUAL expr IN expr { LetExpr ($2, $4, $6) }
    | VAR LPAREN expr_list RPAREN { Apply($1, $3) }
    | LB BAR expr_list BAR RB { ArrayLiteral($3) }

expr_list:
   { [] }
    | expr { [$1] }
    | expr COMMA expr_list { $1 :: $3 }

fun_args:
    { [] }
    | argument { [$1] }
    | argument COMMA fun_args { $1 :: $3 }

argument:
    VAR COLON ty { ($1, $3) }

ty:
    TBOOL { TyBool }
    | TINT { TyInt }
    | ty TARROW ty { TyArrow($1, $3) }
    | LPAREN RPAREN { TyUnit }
    | LPAREN ty RPAREN { $2 }
    | ty ARRAY { TyArray($1) }
