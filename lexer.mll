{
    open Parser
    exception Eof
}

let var = ['a' - 'z' 'A' - 'Z']+

rule token = parse
    [' ' '\t' '\r' '\n']     { token lexbuf }     (* skip blanks *)
    | ['0'-'9']+ as lxm { INT(int_of_string lxm) }
    | '+'            { PLUS }
    | '-'            { MINUS }
    | '*'            { TIMES }
    | '/'            { DIV }
    | '('            { LPAREN }
    | ')'            { RPAREN }
    | "int" { TINT }
    | "bool" { TBOOL }
    | "true" { TRUE }
    | "false" { FALSE }
    | "def" { FUNDEF }
    | "if" { IF }
    | "then" { THEN }
    | "else" { ELSE }
    | "end" { END }
    | "let" { LET }
    | "fun" { FUN }
    | ":" { COLON }
    | ";" { SEMI }
    | ";;" { SEMICOLON2 }
    | "->" { TARROW }
    | "=>" { EQARROW }
    | "<-" { LARROW }
    | "=" { EQUAL }
    | "<" { LT }
    | ">" { GT }
    | "," { COMMA }
    | "." { DOT }
    | "in" { IN }
    | "[" { LB }
    | "]" { RB }
    | "|" { BAR }
    | "do" { DO }
    | "done" { DONE }
    | "while" { WHILE }
    | "array" { ARRAY }
    | var { VAR (Lexing.lexeme lexbuf) }
    | eof { EOF }
