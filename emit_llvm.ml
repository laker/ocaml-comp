exception Error of string

type vardesc = {
    ty: Core.ty;
    llval: Llvm.llvalue;
    symname: string
}

type env_type = (string * vardesc) list

let print_toplevel_defs defs =
   List.iter (fun def ->
       print_endline (Core.toplevel_to_string def)
   ) defs;
   flush stdout

let context = Llvm.global_context ()
let our_module = Llvm.create_module context "our module"
let builder = Llvm.builder context
let i8 = Llvm.i8_type context
let i32 = Llvm.i32_type context
let i64 = Llvm.i64_type context
let i1 = Llvm.i1_type context
let i8p = Llvm.pointer_type i8
let ftype = Llvm.var_arg_function_type i32 [| i8p |];;
let fmt = Llvm.const_string context "%d\n0";;
let gfmt = Llvm.define_global "fmt" fmt our_module;;
let printf = Llvm.declare_function "printf" ftype our_module;;

let empty_env : env_type = []

let funcount = ref 0
let pickFreshName () : string =
    funcount := !funcount + 1;
    "Lambda_" ^ (string_of_int !funcount)

exception OperationNotImplemented

let llvm_type = function
    | Core.TyInt -> Llvm.i32_type context
    | Core.TyBool -> Llvm.i1_type context
    | Core.TyArrow (a,b) -> raise OperationNotImplemented
    | Core.TyUnit -> Llvm.void_type context
    | Core.TyArray t -> raise OperationNotImplemented
    | Core.TyVar  -> raise OperationNotImplemented

let build_arg_env (args: (string * Core.ty) list)(params: Llvm.llvalue array) : env_type =
    let x = ref 0 in
    let build_store = fun (name, ty) ->
        let old = params.(!x) in
        let llval = Llvm.build_alloca (llvm_type ty) name builder in
        Llvm.build_store old llval builder;
        x := !x + 1;
        (name, { ty = ty; llval = llval; symname = name }) in
    List.map build_store args

(* compilation of function arguments *)
let compile_args (fun_def: Llvm.llvalue)(args: (string * Core.ty) list) : env_type =
    let params = Llvm.params fun_def in
    let args_arr = Array.of_list args in
    let set_value_name = fun i a ->
        let (name, _) = args_arr.(i) in
        Llvm.set_value_name name a in
    Array.iteri set_value_name params;
    build_arg_env args params

let print_env env =
    if (List.length env) = 0 then
        print_endline "EMPTY ENV!"
    else
        List.iter (fun (name, { ty = ty }) ->
            print_endline ( name ^ ":" ^ (Core.type_to_str ty) )
        ) env

let rec compile_expr (env: env_type) = function
    | Core.Fun (name, args, ret_ty, body) ->
            let args_arr = Array.of_list args in
            let args_tys = Array.map (fun (name, ty) -> llvm_type ty) args_arr in
            let ll_def = Llvm.function_type (llvm_type ret_ty) args_tys in
            let fun_def = Llvm.declare_function name ll_def our_module in
            let bb = Llvm.append_block context "entry" fun_def in
            Llvm.position_at_end bb builder;
            let nenv = compile_args fun_def args in
            let ret_val = compile_expr (nenv @ env) body in
            let _ = Llvm.build_ret ret_val builder in
            fun_def
    | Core.Lambda (args, ret_ty, body) ->
            let llvm_tys = args |> Array.of_list |> Array.map (fun (name, ty) -> llvm_type ty) in
            let ll_def = Llvm.function_type (llvm_type ret_ty) llvm_tys in
            let name = pickFreshName () in
            let fun_def = Llvm.declare_function name ll_def our_module in
            let bb = Llvm.append_block context "entry" fun_def in
            Llvm.position_at_end bb builder;
            let nenv = compile_args fun_def args in
            let ret_val = compile_expr (nenv @ env) body in
            let _ = Llvm.build_ret ret_val builder in
            fun_def
    | Core.BinaryOp (op, left, right) ->
            let left_val = compile_expr env left in
            let right_val = compile_expr env right in
            (match op with
                | Core.Add -> Llvm.build_add left_val right_val "add" builder
                | Core.Sub -> Llvm.build_sub left_val right_val "sub" builder
                | Core.Mul -> Llvm.build_mul left_val right_val "mul" builder
                | Core.Div -> Llvm.build_sdiv left_val right_val "div" builder
                | Core.Eq -> Llvm.build_icmp Llvm.Icmp.Eq left_val right_val "icmp" builder
                | Core.Lt -> Llvm.build_icmp Llvm.Icmp.Slt left_val right_val "slt" builder
                | Core.Gt -> Llvm.build_icmp Llvm.Icmp.Sgt left_val right_val "sgt" builder)
    | Core.If (cond, then_, else_) ->
            let cond = compile_expr env cond in
            let istrue = Llvm.const_int i1 1 in
            let cond_val = Llvm.build_icmp Llvm.Icmp.Eq cond istrue "ifcond" builder in
            let start_bb = Llvm.insertion_block builder in
            let the_function = Llvm.block_parent start_bb in
            let then_bb = Llvm.append_block context "then" the_function in
            Llvm.position_at_end then_bb builder;
            let then_val = compile_expr env then_ in
            let new_then_bb = Llvm.insertion_block builder in
            (match else_ with
                | Some e ->
                    let else_ = e in
                    let else_bb = Llvm.append_block context "else" the_function in
                    Llvm.position_at_end else_bb builder;
                    let else_val = compile_expr env else_ in
                    let new_else_bb = Llvm.insertion_block builder in
                    let merge_bb = Llvm.append_block context "ifcont" the_function in
                    Llvm.position_at_end merge_bb builder;
                    let incoming = [(then_val, new_then_bb); (else_val, new_else_bb)] in
                    let phi = Llvm.build_phi incoming "iftmp" builder in
                    Llvm.position_at_end start_bb builder;
                    ignore (Llvm.build_cond_br cond_val then_bb else_bb builder);
                    Llvm.position_at_end new_then_bb builder;
                    ignore (Llvm.build_br merge_bb builder);
                    Llvm.position_at_end new_else_bb builder;
                    ignore (Llvm.build_br merge_bb builder);
                    Llvm.position_at_end merge_bb builder;
                    phi
                | None ->
                        istrue)
    | Core.Bool b ->
            if b then
                Llvm.const_int i1 1
            else
                Llvm.const_int i1 0
    | Core.Int i ->
            Llvm.const_int i32 i
    | Core.Apply (callee, args) ->
            if callee = "printf" then
                let args = List.map (fun arg -> compile_expr env arg) args in
                let path = [| (Llvm.const_int i32 0); (Llvm.const_int i32 0) |] in
                let gep =  Llvm.const_gep gfmt path in
                Llvm.build_call printf [| gep ; (Array.of_list args).(0) |] "printfff" builder
            else
                let callee = match Llvm.lookup_function callee our_module with
                    | Some callee -> callee
                    | None -> raise (Error "unbound function: ") in
                let args = List.map (fun arg -> compile_expr env arg) args in
                Llvm.build_call callee (Array.of_list args) "calltmp" builder
    | Core.LetExpr (name, exp, body) ->
            let exp = compile_expr env exp in
            let place = Llvm.build_alloca i32 name builder in
            let store = Llvm.build_store exp place builder in
            let new_env = (name, { symname = "Lambda_1"; ty = Core.TyArrow(Core.TyInt, Core.TyInt); llval = place }) :: env in
            compile_expr new_env body
    | Core.Var name ->
            (try
                let vardesc = List.assoc name env in
                Llvm.build_load vardesc.llval name builder
            with Not_found ->
                failwith ("Variable was not found: " ^ name))
    | Core.ArrayLiteral exprs ->
            let len = List.length exprs in
            let vall = compile_expr env (List.hd exprs) in
            Llvm.build_array_malloc (llvm_type Core.TyInt) vall "array_malloc" builder
    | Core.ExprSeq _
    | Core.While _
    | Core.ArraySet _
    | Core.ArrayGet _ -> raise OperationNotImplemented


let compile_def name e =
    failwith "Cannot compile a def yet!"

let compile = function
    | Core.Expr e -> compile_expr empty_env e
    | Core.Def (name, e) -> compile_def name e

let parse program =
    let lexbuf = Lexing.from_string program in
    try
        Parser.toplevel_def Lexer.token lexbuf
    with Parsing.Parse_error ->
        let pos_start = Lexing.lexeme_start_p lexbuf in
        let pos_end = Lexing.lexeme_end_p lexbuf in
        let lex_name = Lexing.lexeme lexbuf in
        let line_pos = string_of_int pos_start.pos_lnum in
        let err = "Parse error at symbol '" ^ lex_name ^ "' line no: " ^ line_pos in
        print_endline err;
        raise Lexer.Eof

let _ =
    try
        let filename = "program.txt" in
        let lines = ref [] in
        let chan = open_in filename in
        let file_lines = try
            while true do
                lines := input_line chan :: !lines
            done;
            !lines
        with
            End_of_file -> close_in chan; List.rev !lines
        in
        let program = String.concat "\n" file_lines in
        let toplevel_defs = List.rev (parse program) in
        print_endline "toplevel defs: \n";
        print_toplevel_defs toplevel_defs;
        let last = toplevel_defs |> List.map compile |> List.rev |> List.hd in
        Llvm.dump_value last;
        Llvm_bitwriter.write_bitcode_file our_module "our_module.bc";
        Llvm.dump_module our_module;
        (match Llvm_analysis.verify_module our_module with
            | None -> ()
            | Some r -> failwith r);
    with Lexer.Eof ->
        print_endline "EOF";
        exit 0
